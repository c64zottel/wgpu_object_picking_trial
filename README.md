wgpu_object_picking_trial

This project demonstrates how clicked-objects can be identfied via the color picking method.
Furthermore, it implements a simple presenter program, using [gruml](https://gitlab.com/c64zottel/gruml-parser) to
define the presentation and [scm-suite](https://gitlab.com/c64zottel/scm-suite) to keep track of the programs states.


Now, this code is abandoned, kept for historically reasons. Ideas and learnings find place in a new project.