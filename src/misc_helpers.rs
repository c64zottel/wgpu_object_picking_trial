use winit::dpi::PhysicalPosition;

pub(crate) fn get_256_byte_aligned(number: u32) -> u32 {
    let remainder = number % 256;
    if remainder == 0 { return number; }

    (number / 256 + 1) * 256
}


pub(crate) fn object_at_position(position: &PhysicalPosition<f64>, window_inner_size: &(u32, u32), buffer: &Vec<u8>) -> Option<(u32, u32, u32)> {
    let aligned_buffer_width = get_256_byte_aligned(window_inner_size.0 * 16);

    let x = position.x.floor() as u32;
    let y = position.y.floor() as u32;
    let index_y = y * (aligned_buffer_width / 4);
    let index = y * (aligned_buffer_width / 4) + (x * 4);

    let id_buffer: &[u32] = bytemuck::cast_slice(buffer.as_slice());
    if let Some(value) = id_buffer.get(index as usize) {
        if u32::MAX == *value { return None; }; // we ignore mouse events on the background color
        // println!("mouse position: {:?} and object_id: {:?}, index: {}, index_y: {}", position, value, index, index_y);
        // log::info!("mouse position: {:?} and object_id: {:?}", position, value);
        return Some((*value, index, index_y));
    } else {
        return None;
        // println!("out of bounds: {:?} index: {}", position, index);
    }
}