use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use gruml::gruml_image::Image;
use image::DynamicImage;
use wgpu::{Buffer, Device};
use wgpu::util::DeviceExt;
use crate::gpu::gpu_texture::TextureAtlas1D;

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct Vertex {
    position: [f32; 3],
    tex_coords: [f32; 2], // NEW!
}

impl Debug for Vertex {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        // f.write_str(format!( "position {:?}\n", self.position).as_str()).unwrap();
        f.write_str(format!("coord {:?}", self.tex_coords).as_str())
    }
}

#[derive(Debug)]
pub struct ImageStorage<'a> {
    pub text: Vec<&'a gruml::gruml_text::Text>,
    pub images: Vec<DynamicImage>,
    vertices: Vec<Vertex>,
    device: &'a Device,
    indices: Vec<u16>,
}


impl ImageStorage<'_> {
    pub fn new<'a>(device: &'a Device, images: &HashMap<u32, &Image>, atlas: &mut TextureAtlas1D, window_inner_size: &(u32, u32)) -> ImageStorage<'a> {
        let mut image_storage = ImageStorage { text: vec![], images: vec![], vertices: vec![], device, indices: vec![] };

        if image_storage.len() >= 1 { panic!("not implemented") }

        images.iter().for_each(|(_, image)| {
            let atlas_position = atlas.add_texture(&image.image.to_rgba8());
            append_vertex(&mut image_storage.vertices, &image.position, atlas.dimension, atlas_position, window_inner_size);
        });

        image_storage
    }


    pub fn get_vertex_buffer(&self) -> Buffer {
        self.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents: bytemuck::cast_slice(self.vertices.as_ref()),
            usage: wgpu::BufferUsages::VERTEX,
        })
    }

    pub fn len(&self) -> u32 {
        self.indices.len() as u32
    }

    pub fn get_index_buffer(&mut self) -> Buffer {
        let n = self.vertices.len() as u16;
        for i in 0..n {
            self.indices.push(i);
            if ((i + 1) % 4) == 0 {
                self.indices.push(u16::MAX);
            }
        }

        // const INDICES: &[u16] = &[0, 1, 2, 3, u16::MAX, 4, 5, 6, 7, u16::MAX, 8, 9, 10, 11];
        // let num_indices = INDICES.len() as u32;
        self.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(self.indices.as_ref()),
            usage: wgpu::BufferUsages::INDEX,
        })
    }
}


fn append_vertex(vertices: &mut Vec<Vertex>, position: &[u16; 3], atlas_dimension: (u32, u32), atlas_start_end: (u32, u32), window_inner_size: &(u32, u32)) {
    let dimension = [atlas_start_end.1 - atlas_start_end.0, atlas_dimension.1];

    let vertex_l_x = position[0] as f32 / window_inner_size.0 as f32 / 0.5 - 1.0;
    let vertex_r_x = vertex_l_x + dimension[0] as f32 / window_inner_size.0 as f32 / 0.5;
    let vertex_b_y = 1.0 - position[1] as f32 / window_inner_size.1 as f32 / 0.5;
    let vertex_t_y = vertex_b_y - dimension[1] as f32 / window_inner_size.1 as f32 / 0.5;

    let z = position[2] as f32 / u16::MAX as f32;

    let atlas_x_rezip = 1.0 / atlas_dimension.0 as f32;

    let coordinate_x_start = atlas_start_end.0 as f32 * atlas_x_rezip;
    let coordinate_x_end = atlas_start_end.1 as f32 * atlas_x_rezip;

    let coordinate_y_start = 0.0f32;
    let coordinate_y_end = 1.0f32;

    vertices.push(Vertex { position: [vertex_r_x, vertex_t_y, z, ], tex_coords: [coordinate_x_end, coordinate_y_end] });
    vertices.push(Vertex { position: [vertex_l_x, vertex_t_y, z, ], tex_coords: [coordinate_x_start, coordinate_y_end] });
    vertices.push(Vertex { position: [vertex_r_x, vertex_b_y, z, ], tex_coords: [coordinate_x_end, coordinate_y_start] });
    vertices.push(Vertex { position: [vertex_l_x, vertex_b_y, z, ], tex_coords: [coordinate_x_start, coordinate_y_start] });
}