#![allow(dead_code)]
pub const SM_1: &str = r#"
- StateMachine: SM1
  initial_transition:
    target: StartScreen
    executable_content:
      - script: "print(__event.name);"
      - script: "let x = 1"
  states:
    - State:
        id: StartScreen
        on_entry:
          - send:
              string: |
                Rectangle { id: master; position: 0 0 0; dimension: 1920 1080; color: white;
                Rectangle { id: backward; position:   50 1000 0; dimension: 40 20; color: darkgreen; Text { text: "<<"; position:   50 1000 0 } }
                Rectangle { id: forward;  position: 1830 1000 0; dimension: 40 20; color: darkblue; Text { text: ">>"; position: 1830 1000 0 } }
                }}
        transitions:
          - event: { event: pressed, argument: "" }
            target: S1
            condition: print(__event.argument); if __event.argument == "forward" { true } else { false }

    - State:
        id: S1
        on_entry:
          - send:
              string: |
                Rectangle { id: master; position: 0 0 0; dimension: 1920 1080; color: white;
                  Rectangle { id: backward; position:   50 1000 0; dimension: 40 20; color: lightgreen; Text { id: t1; text: "<<"; position:   50 1000 0 } }
                  Rectangle { id: forward;  position: 1830 1000 0; dimension: 40 20; color: red; Text { id: t2; text: ">>"; position: 1830 1000 0 } }
                }}
        transitions:
          - event: { event: pressed, argument: "" }
            target: StartScreen
            condition: if __event.argument == "backward" { true } else { false }
          - event: { event: pressed, argument: "" }
            target: S2
            condition: if __event.argument == "forward" { true } else { false }


    - State:
        id: S2
        transitions:
          - event: { event: pressed, argument: "no reasons" }
            target: StartScreen
            executable_content:
              - send: { string: "Rectangle { id: backward; position: 50 80 2; dimension: 20 10; color: blue;  }" }
              - log: { message: "leaving S2" }

- StateMachine: SM2
  initial_transition:
    target: S1
    executable_content:
      - log: { message: started }
  states:
    - State:
        id: S1
        transitions:
          - event: { event: finish, argument: "no reasons" }
            target: finished
          - event: { event: pressed, argument: "no reasons" }
            target: S1
            executable_content:
              - send: { string: "Rectangle { id: r8; position: 50 80 2; dimension: 300 100; color: red;  }" }
              - log: { message: "round trip to S1" }
              - raise: { machine: SM1, event: { event: pressed, argument: "no reasons" }, delay: 1000 }
    - State:
        id: finished
        on_entry:
          - shutdown: { comment: "done" }
"#;
