use std::collections::HashMap;
use gruml::gruml_item::GrumlItem;
use indextree::{Arena, NodeId};
use wgpu::{Buffer, Device, RenderPipeline, TextureFormat};
use wgpu::util::DeviceExt;
use crate::gpu_rectangle::rectangle_storage::{Rect, RectangleStorage};
use crate::uniform_color::{UniformColor, UniformColorData};

pub struct RectangleRenderer<'a> {
    // pipeline: RenderPipeline,
    device: &'a Device,
    storage: RectangleStorage,
    pub color_data: UniformColorData,
    pub uniform_color: UniformColor,
    pub vertex_buffer: Buffer,
    pub index_buffer: Buffer,
    pub attribute_buffer: Buffer,
}

fn get_rec(device: &Device, arena: &Arena<GrumlItem>, window_inner_size: &(u32, u32)) -> (RectangleStorage, UniformColorData, HashMap<u32, NodeId>) {
    let mut uniform_ = UniformColorData::new(device);
    let mut rs = RectangleStorage::new();

    let mut ids = HashMap::<u32, NodeId>::new();

    arena.iter().enumerate().for_each(|(uid, node)| {
        ids.insert(uid as u32, arena.get_node_id(node).unwrap());
        match node.get() {
            GrumlItem::Rectangle(rec) => {
                let mut gpu_rec = rs.create_rectangle(uid as u32);
                gpu_rec.set_vertices(&rec.position, &rec.dimension, window_inner_size);
                gpu_rec.set_color(rec.color(), &mut uniform_);
            }
            _ => {}
        }
    });

    (rs, uniform_, ids)
}

impl<'a> RectangleRenderer<'a> {
    pub fn new(device: &'a Device, window_inner_size: &(u32, u32), arena: &Arena<GrumlItem>) -> (HashMap<u32, NodeId>, RectangleRenderer<'a>) {
        let (storage, mut color_data, ids) = get_rec(device, arena, window_inner_size);

        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor { label: Some("Rectangle Vertex Buffer"), contents: bytemuck::cast_slice(storage.vertex_buffer()), usage: wgpu::BufferUsages::VERTEX });
        let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor { label: Some("Rectangle Index Buffer"), contents: bytemuck::cast_slice(storage.index_buffer()), usage: wgpu::BufferUsages::INDEX });
        let attribute_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor { label: Some("Rectangle Attribute Buffer"), contents: bytemuck::cast_slice(storage.attribute_buffer()), usage: wgpu::BufferUsages::VERTEX });
        let uniform_color = UniformColor::new(&device, &mut color_data);

        (ids, RectangleRenderer { device, storage, color_data, vertex_buffer, index_buffer, attribute_buffer, uniform_color })
    }

    pub fn get_pipeline(&self, surface_texture_format: TextureFormat) -> RenderPipeline {
        Rect::get_pipeline(&self.device, &self.color_data.bind_group_layout, surface_texture_format)
    }

    pub fn length(&self) -> u32 {
        self.storage.len()
    }
}
