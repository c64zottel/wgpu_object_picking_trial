struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) color: u32,
    @location(2) object_id: u32,
};

struct ColorUniform  {
    colors: array<vec4<f32>,16>
}

@group(0) @binding(0)
var<uniform> colors: ColorUniform;

@vertex
fn vs_main(
    vertex_input: VertexInput,
    @builtin(vertex_index) VertexIndex : u32
) -> VertexOutput {

    var out: VertexOutput;
    out.color = colors.colors[vertex_input.color].xyz;
    out.pos = vec4<f32>(vertex_input.position, 1.0);
    out.object_id = vertex_input.object_id;

    return out;
}

struct VertexOutput {
 @builtin(position)                pos: vec4<f32>,
 @location(0) @interpolate(flat)   object_id: u32,
 @location(1)                      color: vec3<f32>,
}


struct FragmentOutput {
 @location(0) color: vec4<f32>,
 @location(1) object_id: vec4<u32>,
}

@fragment
fn fs_main(input: VertexOutput) -> FragmentOutput {

  var output: FragmentOutput;

  output.color = vec4(input.color, 1.0);
  output.object_id    = vec4(input.object_id, 13u, 12u, 17u); // padding with radonm numbers
  return output;
}
