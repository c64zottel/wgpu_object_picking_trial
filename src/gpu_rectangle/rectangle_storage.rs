use std::mem::size_of;
use wgpu::{TextureFormat, VertexAttribute};
use crate::uniform_color::UniformColorData;
use std::borrow::Cow;
use wgpu::{BindGroupLayout, Device, RenderPipeline, VertexBufferLayout};
use wgpu::IndexFormat::Uint16;
use wgpu::PrimitiveTopology::TriangleStrip;

// pre allocate space for so many rectangles.
const RECTANGLE_COUNT: usize = 512;

//x, y, z
const DIMENSIONS: usize = 3;

// (x, y), (x + width, y), (x, y + height), (x + width, y + height)
const CORNERS_PER_RECTANGLE: usize = 4;

const VERTEX_RECTANGLE_STRIDE_SIZE: usize = DIMENSIONS * CORNERS_PER_RECTANGLE;
const STRIDE_SIZE_IN_BYTES: usize = DIMENSIONS * size_of::<f32>();
const RECTANGLE_ARRAY_ELEMENTS: usize = RECTANGLE_COUNT * DIMENSIONS;

// color index | id
const ATTRIBUTE_ELEMENTS_PER_STRIDE: usize = 2;
const ATTRIBUTE_RECTANGLE_STRIDE_SIZE: usize = ATTRIBUTE_ELEMENTS_PER_STRIDE * CORNERS_PER_RECTANGLE;
// we need 1 ATTRIBUTE per CORNER
const ATTRIBUTE_SIZE_IN_BYTES: usize = ATTRIBUTE_ELEMENTS_PER_STRIDE * size_of::<u32>();
const ATTRIBUTE_ARRAY_ELEMENTS: usize = RECTANGLE_COUNT * ATTRIBUTE_RECTANGLE_STRIDE_SIZE;

// 1 elements
#[allow(dead_code)]
const ATTRIBUTE_COLOR_INDEX: usize = 0;
// 1 element
const ATTRIBUTE_OBJECT_ID: usize = 1;

pub struct RectangleStorage {
    vertices: [f32; RECTANGLE_ARRAY_ELEMENTS],
    indices: [u16; RECTANGLE_ARRAY_ELEMENTS],
    attributes: [u32; ATTRIBUTE_ARRAY_ELEMENTS],
    indices_index: u16,
    next_slice: usize,
    rectangle_count: usize,
}

impl RectangleStorage {
    pub(crate) fn index_buffer(&self) -> &[u16] {
        self.indices.as_ref()
    }

    pub(crate) fn len(&self) -> u32 {
        (self.rectangle_count * (CORNERS_PER_RECTANGLE + 1)) as u32
    }

    pub fn stride_size() -> usize {
        STRIDE_SIZE_IN_BYTES
    }

    pub fn attribute_stride() -> usize {
        ATTRIBUTE_SIZE_IN_BYTES
    }

    pub fn vertex_buffer(&self) -> &[f32] {
        self.vertices.as_ref()
    }

    pub fn attribute_buffer(&self) -> &[u32] {
        self.attributes.as_ref()
    }

    pub fn new() -> RectangleStorage {
        Self { vertices: [0.0; RECTANGLE_ARRAY_ELEMENTS], indices: [0u16; RECTANGLE_ARRAY_ELEMENTS], attributes: [0u32; ATTRIBUTE_ARRAY_ELEMENTS], indices_index: 0, next_slice: 0, rectangle_count: 0 }
    }

    pub fn create_rectangle<'a>(&mut self, oid: u32) -> Rect {
        let slice_start = self.next_slice;
        let slice_end = self.next_slice + VERTEX_RECTANGLE_STRIDE_SIZE;
        let vertex_slice = &mut self.vertices[slice_start..slice_end];

        let attribute_slice_start = self.rectangle_count * ATTRIBUTE_RECTANGLE_STRIDE_SIZE;
        let attribute_slice_end = attribute_slice_start + ATTRIBUTE_RECTANGLE_STRIDE_SIZE;
        let attribute_slice = &mut self.attributes[attribute_slice_start..attribute_slice_end];

        // every other item, starting at 1 (ATTRIBUTE_OBJECT_ID), is a uid - 4 vertices in total
        attribute_slice.iter_mut().skip(ATTRIBUTE_OBJECT_ID).step_by(2).for_each(|e| *e = oid);


        // for draw_indexed()
        for _ in 0..CORNERS_PER_RECTANGLE {
            self.indices[self.rectangle_count + self.indices_index as usize] = self.indices_index;
            self.indices_index += 1;
        }
        self.indices[self.rectangle_count + self.indices_index as usize] = u16::MAX;


        self.next_slice = slice_end;
        self.rectangle_count += 1;

        Rect { vertices_start: vertex_slice, attribute_start: attribute_slice }
    }

    fn vertices() -> [VertexAttribute; 1] {
        [
            VertexAttribute {
                offset: 0,
                shader_location: 0,
                format: wgpu::VertexFormat::Float32x3,
            }
        ]
    }

    // offset is the offset inside a single stride.
    fn attributes() -> [VertexAttribute; 2] {
        [
            VertexAttribute { // color
                offset: 0,
                shader_location: 1,
                format: wgpu::VertexFormat::Uint32,
            },
            VertexAttribute { // object id
                offset: size_of::<u32>() as wgpu::BufferAddress,
                shader_location: 2,
                format: wgpu::VertexFormat::Uint32,
            }
        ]
    }
}

#[repr(C)]
pub struct Rect<'a> {
    vertices_start: &'a mut [f32],
    attribute_start: &'a mut [u32],
}

impl Rect<'_> {
    pub fn set_vertices(&mut self, position: &[u16; 3], dimension: &[u16; 2], window_inner_size: &(u32, u32))
    {
        let vertex_l_x = position[0] as f32 / window_inner_size.0 as f32 / 0.5 - 1.0;
        let vertex_r_x = vertex_l_x + dimension[0] as f32 / window_inner_size.0 as f32 / 0.5;
        let vertex_u_y = 1.0 - position[1] as f32 / window_inner_size.1 as f32 / 0.5;
        let vertex_l_y = vertex_u_y - dimension[1] as f32 / window_inner_size.1 as f32 / 0.5;

        let z = position[2] as f32 / u16::MAX as f32;

        self.vertices_start[0..(4 * DIMENSIONS)].copy_from_slice(&[
            vertex_r_x, vertex_u_y, z,
            vertex_l_x, vertex_u_y, z,
            vertex_r_x, vertex_l_y, z,
            vertex_l_x, vertex_l_y, z,
        ]);
    }

    pub fn set_color(&mut self, color: &[u8; 3], uniform_: &mut UniformColorData) {
        let color_index = uniform_.color_to_id(*color);

        // every other item, starting at 0, is a color id - 4 vertices in total
        self.attribute_start.iter_mut().step_by(2).for_each(|c| { *c = color_index; });
    }

    pub fn get_pipeline(device: &Device, uniform_bind_group_layout: &BindGroupLayout, surface_texture_format: TextureFormat) -> RenderPipeline {
        let vb_layout = VertexBufferLayout {
            array_stride: RectangleStorage::stride_size() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &RectangleStorage::vertices(),
            // macro version: attributes: &wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x3],
        };

        let attribute_layout = VertexBufferLayout {
            array_stride: RectangleStorage::attribute_stride() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &RectangleStorage::attributes(),
            // macro version: attributes: &wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x3],
        };


        let fragment_color_target_states = [
            Some(wgpu::ColorTargetState { format: surface_texture_format, blend: None, write_mask: wgpu::ColorWrites::ALL }),
            Some(wgpu::ColorTargetState { format: TextureFormat::Rgba32Uint, blend: None, write_mask: wgpu::ColorWrites::ALL }),
        ];

        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(include_str!("rectangle.wgsl"))),
        });

        let render_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Render rectangle Pipeline Layout"),
            bind_group_layouts: &[&uniform_bind_group_layout],
            push_constant_ranges: &[],
        });

        device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Rectangle Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[vb_layout, attribute_layout],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &fragment_color_target_states,
            }),
            primitive: wgpu::PrimitiveState {
                topology: TriangleStrip,
                strip_index_format: Some(Uint16),
                ..Default::default()
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState::default(),
            multiview: None,
        })
    }
}

