use std::collections::HashMap;
use gruml::gruml_item::GrumlItem;
use indextree::{Arena, NodeId};
use wgpu::TextureViewDescriptor;
use crate::background_id_texture::GpuTexture;
use crate::gpu_image::image_renderer::ImageRenderer;
use crate::gpu_picking_buffer::get_mrt_buffer;
use crate::gpu_rectangle::rectangle_renderer::RectangleRenderer;
use crate::gpu_setup::GpuSetup;
use crate::gpu_text::text_renderer::TextRenderer;

pub fn draw(gpu_setup: &GpuSetup, arena: &Arena<GrumlItem>) -> (Vec<u8>, HashMap<u32, NodeId>) {
    let screen_surface = gpu_setup.surface_texture();
    let screen_view = screen_surface.texture.create_view(&TextureViewDescriptor::default());

    let id_texture = GpuTexture::new(&gpu_setup);

    let (ids, rectangle_renderer) = RectangleRenderer::new(&gpu_setup.device, &gpu_setup.window_inner_size(), &arena);
    let text_renderer = TextRenderer::new(&gpu_setup.device, &gpu_setup.window_inner_size(), &gpu_setup.queue, include_bytes!("/usr/share/fonts/truetype/ubuntu/Ubuntu-L.ttf"), &arena);
    let image_renderer = ImageRenderer::new(&gpu_setup.device, &gpu_setup.window_inner_size(), &gpu_setup.queue, arena);

    let text_pipeline = text_renderer.get_pipeline(gpu_setup.surface_texture_format());
    let rectangle_pipeline = rectangle_renderer.get_pipeline(gpu_setup.surface_texture_format());
    let image_pipeline = image_renderer.get_pipeline(gpu_setup.surface_texture_format());

    let mut encoder = gpu_setup.device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: Some("cmd encoder - rectangle") });
    {
        let render_pass_desc = wgpu::RenderPassDescriptor {
            label: Some("Render Pass"),
            color_attachments: &[
                Some(wgpu::RenderPassColorAttachment {
                    view: &screen_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color { r: 0.7, g: 0.2, b: 0.3, a: 1.0 }),
                        store: true,
                    },
                }),
                Some(wgpu::RenderPassColorAttachment {
                    view: &id_texture.texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color { r: f64::MAX, g: f64::MAX, b: f64::MAX, a: f64::MAX }),
                        store: true,
                    },
                }),
            ],
            depth_stencil_attachment: None,
        };
        let mut render_pass = encoder.begin_render_pass(&render_pass_desc);

        render_pass.set_pipeline(&rectangle_pipeline);
        render_pass.set_bind_group(0, &rectangle_renderer.uniform_color.bind_group, &[]);
        render_pass.set_index_buffer(rectangle_renderer.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
        render_pass.set_vertex_buffer(0, rectangle_renderer.vertex_buffer.slice(..));
        render_pass.set_vertex_buffer(1, rectangle_renderer.attribute_buffer.slice(..));
        render_pass.draw_indexed(0..rectangle_renderer.length(), 0, 0..1);


        if image_renderer.has_data {
            render_pass.set_pipeline(&image_pipeline);
            render_pass.set_bind_group(0, &image_renderer.bind_group, &[]);
            render_pass.set_vertex_buffer(0, image_renderer.vertex_buffer.slice(..));
            render_pass.set_index_buffer(image_renderer.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
            render_pass.draw_indexed(0..image_renderer.num_indices, 0, 0..1);
        }

        render_pass.set_pipeline(&text_pipeline);
        render_pass.set_bind_group(0, &text_renderer.bind_group, &[]);
        render_pass.set_vertex_buffer(0, text_renderer.vertex_buffer.slice(..));
        render_pass.set_index_buffer(text_renderer.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
        render_pass.draw_indexed(0..text_renderer.num_indices, 0, 0..1);
    }


    let buffer = get_mrt_buffer(&gpu_setup, &id_texture, encoder, screen_surface);

    (buffer, ids)
}