use gruml::constructors::gruml_parser;
use scm_lib::SMManager;
use winit::dpi::PhysicalPosition;
use winit::event::{ElementState, Event, KeyboardInput, MouseButton, TouchPhase, VirtualKeyCode, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::Fullscreen;
use crate::gpu_setup::GpuSetup;
use crate::grml_samples;
use crate::misc_helpers::object_at_position;


pub fn run(event_loop: EventLoop<String>, gpu_setup: GpuSetup, runtime: SMManager) {
    let (_, mut arena) = gruml_parser(grml_samples::BG_REC).unwrap();
    let (mut buffer, mut ids) = gpu_setup.draw(&arena);

    runtime.send_event("SM1", "start");

    let mut touch_x_start = 0.0;
    let mut mouse_position = PhysicalPosition::default();
    let mut window_inner_size = gpu_setup.window_inner_size();
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;
        match event {
            Event::UserEvent(s) => {
                (_, arena) = gruml_parser(&s).unwrap();
                (buffer, ids) = gpu_setup.draw(&arena);
            }
            Event::RedrawRequested(window_id) if window_id == gpu_setup.window.id() => {
                (buffer, ids) = gpu_setup.draw(&arena);
            }
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == gpu_setup.window.id() => match event {
                WindowEvent::Touch(touch) => {
                    if touch.phase == TouchPhase::Started {
                        touch_x_start = touch.location.x;
                    }

                    if touch.phase == TouchPhase::Ended {
                        if touch.location.x > touch_x_start {
                            runtime.send_event("SM1", scm_lib::communication::TargetEvent {
                                name: "pressed".to_string(),
                                argument: "backward".to_string(),
                            });
                        } else {
                            runtime.send_event("SM1", scm_lib::communication::TargetEvent {
                                name: "pressed".to_string(),
                                argument: "forward".to_string(),
                            });
                        }
                    }
                }
                #[allow(unused_variables, deprecated)]
                WindowEvent::CursorMoved { device_id, position, modifiers } => {
                    mouse_position = position.clone();
                    // if let Some(x) = object_at_position(&mouse_position, &window_inner_size, &buffer) {
                    //     dbg!("moved: ",  x);
                    // }
                }

                WindowEvent::MouseInput { button, state, .. } => {
                    match button {
                        MouseButton::Left => {
                            if let Some(x) = object_at_position(&mouse_position, &window_inner_size, &buffer) {
                                if *state == ElementState::Pressed {
                                    let node = arena.get(*ids.get(&x.0).unwrap()).unwrap();
                                    runtime.send_event("SM1", scm_lib::communication::TargetEvent {
                                        name: "pressed".to_string(),
                                        argument: node.get().to_string(),
                                    });
                                }
                            }
                        }
                        MouseButton::Right => {}
                        MouseButton::Middle => {}
                        MouseButton::Other(_) => {}
                    }
                }

                WindowEvent::Resized(phy_size) => {
                    gpu_setup.window_resized((phy_size.width, phy_size.height));
                    window_inner_size = gpu_setup.window_inner_size();
                }
                WindowEvent::KeyboardInput {
                    input: KeyboardInput {
                        state: ElementState::Pressed,
                        virtual_keycode: key_code,
                        ..
                    }, ..
                } => {
                    match key_code.unwrap() {
                        VirtualKeyCode::Right | VirtualKeyCode::Space => {
                            runtime.send_event("SM1", scm_lib::communication::TargetEvent {
                                name: "pressed".to_string(),
                                argument: "forward".to_string(),
                            });
                        }
                        VirtualKeyCode::F => {
                            if let Some(_) = gpu_setup.window.fullscreen() {
                                gpu_setup.window.set_fullscreen(None);
                            } else {
                                gpu_setup.window.set_fullscreen(Some(Fullscreen::Borderless(None)));
                            }
                        }
                        VirtualKeyCode::Left => {
                            runtime.send_event("SM1", scm_lib::communication::TargetEvent {
                                name: "pressed".to_string(),
                                argument: "backward".to_string(),
                            });
                        }
                        VirtualKeyCode::Escape => *control_flow = ControlFlow::Exit,
                        _ => {}
                    }
                }

                WindowEvent::CloseRequested {} => *control_flow = ControlFlow::Exit,
                _ => {}
            },
            _ => {}
        }
    });
}