use pollster::FutureExt;
use wgpu::{CommandEncoder, SurfaceTexture};
use crate::background_id_texture::GpuTexture;
use crate::misc_helpers::get_256_byte_aligned;
use crate::gpu_setup::GpuSetup;


pub(crate) fn get_mrt_buffer(gpu_setup: &GpuSetup, gpu_texture: &GpuTexture, mut encoder: CommandEncoder, screen_surface: SurfaceTexture) -> Vec<u8> {
    let window_inner_size = gpu_setup.window_inner_size();
    let aligned_buffer_width = get_256_byte_aligned(window_inner_size.0 * 16);

    let output_buffer_size = (aligned_buffer_width * window_inner_size.1) as wgpu::BufferAddress;
    let output_buffer_desc = wgpu::BufferDescriptor {
        size: output_buffer_size,
        usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::MAP_READ,
        label: None,
        mapped_at_creation: false,
    };

    let output_buffer = gpu_setup.device.create_buffer(&output_buffer_desc);


    let aligned_buffer_width = get_256_byte_aligned(window_inner_size.0 * 16);
    encoder.copy_texture_to_buffer(
        wgpu::ImageCopyTexture {
            aspect: wgpu::TextureAspect::All,
            texture: &gpu_texture.texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        },
        wgpu::ImageCopyBuffer {
            buffer: &output_buffer,
            layout: wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(aligned_buffer_width),
                rows_per_image: None,
            },
        },
        gpu_texture.texture_descriptor.size,
    );

    let index = gpu_setup.queue.submit(Some(encoder.finish()));
    screen_surface.present();

    let buffer = {
        let buffer_slice = output_buffer.slice(..);

        let (tx, rx) = futures_intrusive::channel::shared::oneshot_channel();
        buffer_slice.map_async(wgpu::MapMode::Read, move |result| {
            tx.send(result).unwrap();
        });
        gpu_setup.device.poll(wgpu::Maintain::WaitForSubmissionIndex(index));
        rx.receive().block_on().unwrap().unwrap();

        buffer_slice.get_mapped_range()
    };

    buffer.to_owned()
}