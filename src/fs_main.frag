#version 330 core
struct VertexOutput {
    vec4 pos;
    uint id;
};
struct FragmentOutput {
    vec4 color;
    uint id;
};
flat in uint _vs2fs_location0;
layout(location = 0) out vec4 _fs2p_location0;
layout(location = 1) out uint _fs2p_location1;

void main() {
    VertexOutput input_ = VertexOutput(gl_FragCoord, _vs2fs_location0);
    FragmentOutput output_1 = FragmentOutput(vec4(0.0), 0u);
    output_1.color = vec4(1.0, 0.5, 0.10000000149011612, 1.0);
    output_1.id = input_.id;
    FragmentOutput _e10 = output_1;
    _fs2p_location0 = _e10.color;
    _fs2p_location1 = _e10.id;
    return;
}

