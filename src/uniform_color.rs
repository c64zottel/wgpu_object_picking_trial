use wgpu::{BindGroup, BindGroupLayout, Device};
use wgpu::util::DeviceExt;

// This must be the same size as ColorUniform in the shader
const NUMBER_COLORS: usize = 4 * 16;

pub struct UniformColorData {
    color_palette: Vec<[u8; 3]>,
    color_palette_as_float: [f32; NUMBER_COLORS],
    pub(crate) bind_group_layout: BindGroupLayout,
}

// We store colors in RGB format.
// On upload, those values are transformed to f32
impl UniformColorData {
    pub fn color_to_id(&mut self, color: [u8; 3]) -> u32 {
        let result = self.color_palette.iter().position(|v| {
            v[0] == color[0] && v[1] == color[1] && v[2] == color[2]
        });

        let index = match result {
            None => {
                self.color_palette.push(color);
                self.color_palette.len() - 1
            }
            Some(i) => { i }
        };

        index as u32
    }

    pub fn buffer_as_f32(&mut self) -> &[f32] {
        for (i, v) in self.color_palette.iter().enumerate() {
            self.color_palette_as_float[i * 4 + 0] = (v[0] as f32) * 1.0 / 255.0;
            self.color_palette_as_float[i * 4 + 1] = (v[1] as f32) * 1.0 / 255.0;
            self.color_palette_as_float[i * 4 + 2] = (v[2] as f32) * 1.0 / 255.0;
            self.color_palette_as_float[i * 4 + 3] = 1.0; // padding
        }

        self.color_palette_as_float.as_slice()
    }

    pub fn new(device: &Device) -> Self {
        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }
            ],
            label: Some("uniform bind group layout"),
        });

        Self {
            color_palette: Vec::new(),
            color_palette_as_float: [0f32; NUMBER_COLORS],
            bind_group_layout,
        }
    }
}

pub struct UniformColor {
    pub bind_group: BindGroup,
}

impl UniformColor {
    pub fn new(device: &Device, buffer_data: &mut UniformColorData) -> Self {
        let device_buffer = device.create_buffer_init(
            &wgpu::util::BufferInitDescriptor {
                label: Some("Uniform Color Buffer"),
                contents: bytemuck::cast_slice(&buffer_data.buffer_as_f32()),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            }
        );

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &buffer_data.bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: device_buffer.as_entire_binding(),
                }
            ],
            label: Some("uniform Color bind group"),
        });
        Self { bind_group }
    }
}