
struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) tex_coords: vec2<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
}

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.tex_coords = model.tex_coords;
    out.clip_position = vec4<f32>(model.position, 1.0);
    return out;
}



 // Fragment shader

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0)@binding(1)
var s_diffuse: sampler;

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    var c = textureSample(t_diffuse, s_diffuse, in.tex_coords) ;
    var t = 0.001;
    if ( c.r < t ) {
        discard;
    }
//    if ( c.x < t && c.y < t && c.z < t || c.w < 0.0001 ) {
//        discard;
//    }
//    var o = 0.0;
//    if (c.x > 0.0 )
//        { o = 0.5; }
    return vec4(  0.5 - c.r, 0.5 -  c.g, 0.7 + c.b ,  c.a );
}
