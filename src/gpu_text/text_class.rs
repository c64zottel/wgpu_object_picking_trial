use image::DynamicImage;
use rusttype::{Font, Scale};

pub struct TextClass {
    scale: Scale,
    color: (u8, u8, u8),
}


impl TextClass {
    pub fn new(scale: f32, color: (u8, u8, u8)) -> TextClass {
        let scale = Scale::uniform(scale);
        Self {
            scale,
            color,
        }
    }

    pub fn text_to_image(&self, text: &str, font: &Font) -> DynamicImage {
        let v_metrics = font.v_metrics(self.scale);

        let glyphs: Vec<_> = font
            .layout(text, self.scale, rusttype::point(0.0, 0.0 + v_metrics.ascent))
            .collect();

        // work out the layout size
        let glyphs_height = (v_metrics.ascent - v_metrics.descent).ceil() as u32;
        let glyphs_width = {
            let min_x = glyphs
                .first()
                .map(|g| g.pixel_bounding_box().unwrap().min.x)
                .unwrap();
            let max_x = glyphs
                .last()
                .map(|g| g.pixel_bounding_box().unwrap().max.x)
                .unwrap();
            (max_x - min_x) as u32
        };

        // Create a new rgba, for some reason width must be padded (+1)
        let mut image = image::DynamicImage::new_luma8(glyphs_width + 1, glyphs_height  ).to_luma8();

        // Loop through the glyphs in the text, positing each one on a line
        for glyph in glyphs {
            if let Some(bounding_box) = glyph.pixel_bounding_box() {
                // Draw the glyph into the image per-pixel by using the draw closure
                glyph.draw(|x, y, v| {
                    image.put_pixel(
                        // Offset the position by the glyph bounding box
                        x + bounding_box.min.x as u32,
                        y + bounding_box.min.y as u32,
                        // Turn the coverage into an alpha value
                        //  image::Rgba([self.color.0, self.color.1, self.color.2, (v * 255.0) as u8]),
                        image::Luma([((v * 255.0) as u8)]),
                    )
                });
            }
        }

        image.into()
    }
}