use rusttype::Font;
use wgpu::{BindGroup, Device, Queue, TextureFormat};
use wgpu::{BindGroupLayout, RenderPipeline, VertexBufferLayout};
use wgpu::IndexFormat::Uint16;
use wgpu::PrimitiveTopology::TriangleStrip;
use std::borrow::Cow;
use gruml::gruml_item::GrumlItem;
use indextree::Arena;
use crate::gpu::gpu_texture::TextureAtlas1D;
use crate::gpu_text::text_storage::TextStorage;


pub struct TextRenderer<'a> {
    pub(crate) bind_group: BindGroup,
    pub(crate) vertex_buffer: wgpu::Buffer,
    pub(crate) index_buffer: wgpu::Buffer,
    pub(crate) num_indices: u32,
    pub(crate) bind_group_layout: BindGroupLayout,
    device: &'a Device,
}

impl<'a> TextRenderer<'a> {
    fn bind_group(device: &Device, diffuse_texture: &TextureAtlas1D) -> (BindGroupLayout, BindGroup) {
        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                ],
                label: Some("texture bind group layout"),
            });


        let bind_group = device.create_bind_group(
            &wgpu::BindGroupDescriptor {
                layout: &texture_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::TextureView(&diffuse_texture.view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler),
                    }
                ],
                label: Some("texture bind group"),
            });

        (texture_bind_group_layout, bind_group)
    }


    pub fn new(
        device: &'a Device,
        window_inner_size: &(u32, u32),
        queue: &Queue,
        font_data: &'a [u8],
        arena: &'a Arena<GrumlItem>,
    ) -> TextRenderer<'a> {
        let font = Font::try_from_bytes(font_data).expect("Error constructing Font");

        let mut texture_text_atlas = TextureAtlas1D::new(&device, &queue, 800, 22);
        let mut ts = TextStorage::new(device, &font, arena, &mut texture_text_atlas, window_inner_size);

        let (bind_group_layout, bind_group) =
            Self::bind_group(device, &texture_text_atlas);

        let vertex_buffer = ts.get_vertex_buffer();
        let index_buffer = ts.get_index_buffer();

        Self {
            bind_group,
            vertex_buffer,
            index_buffer,
            num_indices: ts.len(),
            bind_group_layout: bind_group_layout,
            device,
        }
    }


    pub fn get_pipeline(&self, surface_texture_format: TextureFormat) -> RenderPipeline {
        #[repr(C)]
        #[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
        struct Vertex {
            position: [f32; 3],
            tex_coords: [f32; 2], // NEW!
        }
        use std::mem;
        let vb_layout = VertexBufferLayout {
            array_stride: mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x2, // NEW!
                },
            ],
        };


        let fragment_color_target_states = [
            Some(wgpu::ColorTargetState { format: surface_texture_format, blend: None, write_mask: wgpu::ColorWrites::ALL }),
            Some(wgpu::ColorTargetState { format: TextureFormat::Rgba32Uint, blend: None, write_mask: wgpu::ColorWrites::ALL }),
        ];

        let shader = self.device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(include_str!("text.wgsl"))),
        });


        let render_pipeline_layout = self.device.create_pipeline_layout(
            &wgpu::PipelineLayoutDescriptor {
                label: Some("Render text Pipeline Layout"),
                bind_group_layouts: &[&self.bind_group_layout], // NEW!
                push_constant_ranges: &[],
            }
        );

        self.device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Text Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[vb_layout],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &fragment_color_target_states,
            }),
            primitive: wgpu::PrimitiveState {
                topology: TriangleStrip,
                strip_index_format: Some(Uint16),
                ..Default::default()
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState::default(),
            multiview: None,
        })
    }
}

