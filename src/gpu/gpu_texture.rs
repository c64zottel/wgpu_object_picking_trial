use image::RgbaImage;
use wgpu::Extent3d;

pub struct TextureAtlas1D<'a> {
    pub texture: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub sampler: wgpu::Sampler,
    pub dimension: (u32, u32),
    queue: &'a wgpu::Queue,
    current_x_position: u32,
}

impl<'a> TextureAtlas1D<'a> {
    pub fn new(
        device: &wgpu::Device,
        queue: &'a wgpu::Queue,
        width: u32,
        height: u32,
    ) -> TextureAtlas1D<'a> {
        let size = Extent3d { width, height, depth_or_array_layers: 1 };

        let format = wgpu::TextureFormat::Rgba8UnormSrgb;
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some("texture atlas 1d"),
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            view_formats: &[],
        });


        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        Self {
            texture,
            view,
            sampler,
            queue,
            dimension: (width, height),
            current_x_position: 0,
        }
    }

    pub fn add_texture(&mut self, img: &RgbaImage) -> (u32, u32) {
        dbg!(&img.width());
        dbg!(&img.height());
        dbg!(&self.current_x_position);
        let width = img.width();
        let in_atlas_position = (self.current_x_position, self.current_x_position + width);
        self.queue.write_texture(
            wgpu::ImageCopyTexture {
                aspect: wgpu::TextureAspect::All,
                texture: &self.texture,
                mip_level: 0,
                origin: wgpu::Origin3d {
                    x: self.current_x_position,
                    y: 0,
                    z: 0,
                },
            },
            &img,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(4 * width),
                rows_per_image: Some(self.dimension.1),
            },
            Extent3d {
                width,
                height: self.dimension.1,
                depth_or_array_layers: 1,
            },
        );
        self.current_x_position += width;

        in_atlas_position
    }
}