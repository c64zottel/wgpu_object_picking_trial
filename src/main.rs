mod misc_helpers;
mod event_handle;
mod background_id_texture;
mod uniform_color;
mod grml_samples;
mod gpu_text;
mod gpu_rectangle;
mod canvas;
mod gpu_picking_buffer;
mod gpu_setup;
mod statemachine_samples;
mod gpu_image;
mod gpu;

use std::fs::read_to_string;
use scm_lib::init_state_machines_from_json;
use tokio::spawn;
use tokio::sync::mpsc::Receiver;
use winit::event_loop::{EventLoop, EventLoopBuilder, EventLoopProxy};
use winit::window::{Window, WindowBuilder};
use crate::gpu_setup::GpuSetup;
use scm_lib::communication::{InterSMProtocol, ManageSMProtocol, SMProtocol};


async fn worker(event_loop_proxy: EventLoopProxy<String>, mut receiver: Receiver<SMProtocol>) {
    loop {
        if let Some(msg) = receiver.recv().await {
            match msg {
                SMProtocol::InterSMProtocol(msg) => {
                    match msg {
                        InterSMProtocol::RaiseEvent(_) => {}
                        InterSMProtocol::RaiseDelayedEvent(_, _) => {}
                    }
                }
                SMProtocol::ManageSMProtocol(msg) => {
                    match msg {
                        ManageSMProtocol::ShutDown(msg) => {
                            dbg!("received:");
                            dbg!(msg);
                            break;
                        }
                        _ => { dbg!("hä?"); }
                    }
                }
                SMProtocol::CustomMessage(msg) => {
                    dbg!(&msg);
                    event_loop_proxy.send_event(msg).unwrap();
                }
            }
        }
    }
}


pub async fn run(event_loop: EventLoop<String>, window: Window) {
    let gpu_setup = GpuSetup::new(window).await;

    let event_loop_proxy = event_loop.create_proxy();

    let file = read_to_string("presentation.yaml").unwrap();
    let json = serde_yaml::from_str(&file).unwrap();
    // let json = serde_yaml::from_str(statemachine_samples::SM_1).unwrap();
    let (runtime, receiver) = init_state_machines_from_json(json);

    spawn(async move { worker(event_loop_proxy, receiver).await });

    event_handle::run(event_loop, gpu_setup, runtime);
}


#[tokio::main]
async fn main() {
    let event_loop = EventLoopBuilder::<String>::with_user_event().build();
    let window = WindowBuilder::new().with_inner_size(winit::dpi::LogicalSize::new(1920, 1080)).build(&event_loop).unwrap();
    window.set_cursor_icon(winit::window::CursorIcon::Crosshair);
    #[cfg(not(target_arch = "wasm32"))]
    {
        env_logger::init();
        // Temporarily avoid srgb formats for the swapchain on the web
        pollster::block_on(run(event_loop, window));
    }
    #[cfg(target_arch = "wasm32")]
    {
        std::panic::set_hook(Box::new(console_error_panic_hook::hook));
        console_log::init().expect("could not initialize logger");
        use winit::platform::web::WindowExtWebSys;
        // On wasm, append the canvas to the document body
        web_sys::window()
            .and_then(|win| win.document())
            .and_then(|doc| doc.body())
            .and_then(|body| {
                body.append_child(&web_sys::Element::from(window.canvas()))
                    .ok()
            })
            .expect("couldn't append canvas to document body");
        log::info!("hello there: {:?}", window.inner_size());
        wasm_bindgen_futures::spawn_local(run(event_loop, window));
    }
}

