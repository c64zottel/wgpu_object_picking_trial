#![allow(dead_code)]

pub const BG_REC: &str = r#"
Rectangle {
    id: background
    position: 0 0 0
    dimension: 1920 1080
    color: moccasin
    opacity: 0.3
    border.color: black
    border.width: 5
    radius: 10
}
"#;

pub const SAMPLE_REC: &str = r#"
Rectangle {
    id: r1
    position: 5 8 2
    dimension: 30 10
    color: green
    opacity: 0.3
    border.color: black
    border.width: 5
    radius: 10

    Text {
        id: t1
        text: "Hello world"
        position: 10 20 0
        color: blue
        font.pixelSize: 22
    }
    Rectangle {
        id: r4
        opacity: 0.4
        position: 500 140
        dimension: 100 150
        color: red
        border.color: black
        border.width: 5
        radius: 10
        Text {
            id: t2
            text: "Some other text"
            position: 10 50 0
            color: green
            font.pixelSize: 22
        }
    }

    Rectangle {
        id: r2
        opacity: 0.7
        position: 50 8 3
        dimension: 100 150
        color: yellow
        border.color: black
        border.width: 5
        radius: 10
        Rectangle {
            id: r3
            opacity: 0.4
            position: 200 8
            dimension: 200 150
            color: lightblue
            border.color: black
            border.width: 5
            radius: 10
            Text {
                id: t2
                text: "And text again"
                position: 20 100 0
                color: purple
                font.pixelSize: 22
            }
        }
        Rectangle {
            id: r5
            opacity: 0.4
            position: 400 340
            dimension: 200 150
            color: lightgreen
            border.color: black
            border.width: 5
            radius: 10

            Text {
                id: t4
                text: "Who is afraid of vigina wolf"
                position: 200 100 0
                color: green
                font.pixelSize: 22
            }
        }
    }
}
"#;

pub const SAMPLE_REC_1: &str = r#"
Rectangle {
    id: singleBlue
    position: 10 8 2
    dimension: 300 150
    color: blue
    opacity: 0.3
    border.color: black
    border.width: 5
    radius: 10
    Rectangle {
        id: r4
        opacity: 0.4
        position: 500 140
        dimension: 100 150
        color: red
        border.color: black
        border.width: 5
        radius: 10
    }
}
"#;

pub const SAMPLE_REC_2: &str = r#"
Rectangle {
    id: r1
    position: 50 80 2
    dimension: 300 120
    color: lightblue
    opacity: 0.3
    border.color: black
    border.width: 5
    radius: 10

    Text {
        id: t1
        text: "Hello world"
        position: 10 20 0
        color: red
        font.pixelSize: 22
    }
    Rectangle {
        id: r4
        opacity: 0.4
        position: 500 140
        dimension: 100 150
        color: red
        border.color: black
        border.width: 5
        radius: 10
        Text {
            id: t2
            text: "Some other text"
            position: 10 50 0
            color: green
            font.pixelSize: 22
        }
    }

    Rectangle {
        id: r2
        opacity: 0.7
        position: 50 8 3
        dimension: 100 150
        color: yellow
        border.color: black
        border.width: 5
        radius: 10
        Rectangle {
            id: r3
            opacity: 0.4
            position: 200 8
            dimension: 200 150
            color: purple
            border.color: black
            border.width: 5
            radius: 10
            Text {
                id: t2
                text: "And text again"
                position: 20 100 0
                color: purple
                font.pixelSize: 22
            }
        }
        Rectangle {
            id: r5
            opacity: 0.4
            position: 400 340
            dimension: 200 150
            color: lightgreen
            border.color: black
            border.width: 5
            radius: 10

            Text {
                id: t4
                text: "Who is afraid of vigina wolf"
                position: 200 100 0
                color: green
                font.pixelSize: 22
            }
        }
    }
}
"#;