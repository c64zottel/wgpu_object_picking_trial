use wgpu::{TextureDescriptor, TextureFormat};
use crate::gpu_setup::GpuSetup;

fn texture_descriptor_mrt(window_inner_size: (u32, u32)) -> TextureDescriptor<'static> {
    TextureDescriptor {
        size: wgpu::Extent3d {
            width: window_inner_size.0,
            height: window_inner_size.1,
            depth_or_array_layers: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TextureFormat::Rgba32Uint, // 4 * 4 bytes
        usage: wgpu::TextureUsages::COPY_SRC | wgpu::TextureUsages::RENDER_ATTACHMENT,
        label: Some("Texture Descriptor for object_id MRT"),
        view_formats: &[],
    }
}


pub struct GpuTexture<'a> {
    pub(crate) texture_descriptor: TextureDescriptor<'a>,
    pub(crate) texture: wgpu::Texture,
    pub texture_view: wgpu::TextureView,
}

impl GpuTexture<'_> {
    pub fn new(gpu_setup: &GpuSetup) -> Self {
        let texture_descriptor = texture_descriptor_mrt(gpu_setup.window_inner_size());
        let texture = gpu_setup.device.create_texture(&texture_descriptor);
        let texture_view = texture.create_view(&Default::default());

        Self {
            texture_descriptor,
            texture,
            texture_view,
        }
    }
}
