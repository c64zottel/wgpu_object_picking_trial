use std::cell::RefCell;
use std::collections::HashMap;
use gruml::gruml_item::GrumlItem;
use indextree::{Arena, NodeId};
use wgpu::{Device, Queue, Surface, SurfaceConfiguration, SurfaceTexture, TextureFormat};
use winit::window::Window;

#[derive(Debug)]
pub struct GpuSetup {
    pub device: Device,
    pub queue: Queue,
    pub window: Window,
    surface: Surface,
    surface_config: RefCell<SurfaceConfiguration>,
}

impl GpuSetup {
    pub async fn new(window: Window) -> GpuSetup {
        let instance = wgpu::Instance::default();
        let surface = unsafe { instance.create_surface(&window) }.unwrap();
        let adapter = instance.request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: Default::default(),
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        }).await.unwrap();

        let (device, queue) = adapter.request_device(&wgpu::DeviceDescriptor {
            label: Some("main device adapter"),
            features: Default::default(),
            limits: wgpu::Limits::downlevel_webgl2_defaults().using_resolution(adapter.limits()),
        }, None).await.unwrap();

        let window_inner_size = window.inner_size();
        let surface_caps = surface.get_capabilities(&adapter);

        let surface_config = SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface_caps.formats.iter()
                .copied()
                .filter(|f| f.is_srgb())
                .next().unwrap_or(surface_caps.formats[0]),
            width: window_inner_size.width,
            height: window_inner_size.height,
            present_mode: surface_caps.present_modes[0],
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
        };

        surface.configure(&device, &surface_config);

        Self {
            surface,
            device,
            queue,
            surface_config: RefCell::new(surface_config),
            window,
        }
    }

    pub fn window_resized(&self, window_size: (u32, u32)) {
        self.surface_config.borrow_mut().width = window_size.0;
        self.surface_config.borrow_mut().height = window_size.1;
        self.surface.configure(&self.device, &self.surface_config.borrow());
    }

    pub fn draw(&self, arena: &Arena<GrumlItem>) -> (Vec<u8>, HashMap<u32, NodeId>) {
        crate::canvas::draw(&self, arena)
    }


    pub fn window_inner_size(&self) -> (u32, u32) {
        (self.surface_config.borrow().width, self.surface_config.borrow().height)
    }

    pub fn surface_texture_format(&self) -> TextureFormat {
        self.surface_config.borrow().format
    }

    pub fn surface_texture(&self) -> SurfaceTexture {
        self.surface.get_current_texture().unwrap()
    }
}